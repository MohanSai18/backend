const _ = require('lodash');
const Axios = require('axios');
const ffmpeg = require('fluent-ffmpeg');
const fs = require('fs');
const PasswordGenerator = require('strong-password-generator');
const path = require('path');
const PdfThumbnail = require('pdf-thumbnail');
const Thumbnail = require('simple-thumbnail');
const url = require('url');
const uuid = require('uuid').v4;
const ytdl = require('ytdl-core');
const { LinkType } = require('./Constant');
const JwtToken = require('./JwtToken');
const Logger = require('./Logger');
const Message = require('./Message');
const Response = require('./Response');
const { ApiUrl, ContentDir, IsLocal } = require('../config');

class Helper {
  static _buildData(model, params, autoFilled, buildType) {
    const columns = _.chain(params)
      .keys()
      .intersection(model[buildType])
      .difference(model.autoFilledColumns)
      .value();
    if (columns.length) {
      return { ..._.pick(params, model[buildType]), ..._.pick(autoFilled, model[buildType]) };
    }

    return null;
  }

  static BuildCreate(model, params, autoFilled = {}) {
    return Helper._buildData(model, params, autoFilled, 'createColumns');
  }

  static BuildUpdate(model, params, autoFilled = {}) {
    return Helper._buildData(model, params, autoFilled, 'updateColumns');
  }

  static GeneratePassword() {
    if (IsLocal) {
      return '12345678';
    }
    return PasswordGenerator.generatePassword({
      base: 'WORD',
      length: {
        min: 12,
        max: 16,
      },
      capsLetters: {
        min: 3,
        max: 3,
      },
      numerals: {
        min: 2,
        max: 2,
      },
      spacialCharactors: {
        includes: [],
        min: 0,
        max: 0,
      },
      spaces: {
        allow: false,
        min: 0,
        max: 0,
      },
    });
  }

  static IsFileExist(filePath) {
    return filePath && fs.existsSync(filePath) && fs.lstatSync(filePath).isFile();
  }

  static async UnlinkFile(filePath) {
    try {
      Logger.info('Deleting file', { filePath });

      if (filePath && Helper.IsFileExist(filePath)) {
        fs.unlinkSync(filePath);
      }
    } catch (e) {
      Logger.error(`Deleting file ${filePath}`, e);
    }
  }

  static async CreatePdfThumbnail(filePath) {
    try {
      Logger.info('Creating pdf thumbnail', { filePath });

      const pdfBuffer = fs.readFileSync(filePath);
      const thumbnailBuffer = await PdfThumbnail(pdfBuffer, {
        compress: {
          type: 'JPEG',
          quality: 70,
        },
        resize: {
          width: 300,
          height: 300,
        },
      });

      const extWithDot = path.extname(filePath);
      const index = filePath.indexOf(extWithDot);
      const thumbnailPath = `${filePath.substring(0, index - 1)}-thumb.jpeg`;
      thumbnailBuffer.pipe(fs.createWriteStream(thumbnailPath));
      return thumbnailPath;
    } catch (e) {
      Logger.error(`Creating pdf thumbnail ${filePath}`, e);

      throw Response.createError(Message.tryAgain, e);
    }
  }

  static async CreateVideoThumbnail(filePath) {
    try {
      Logger.info('Creating video thumbnail', { filePath });

      const extWithDot = path.extname(filePath);
      const index = filePath.indexOf(extWithDot);
      const thumbnailPath = `${filePath.substring(0, index - 1)}-thumb.png`;

      await Thumbnail(filePath, thumbnailPath, '300x?');

      return thumbnailPath;
    } catch (e) {
      Logger.error(`Creating video thumbnail ${filePath}`, e);

      throw Response.createError(Message.tryAgain, e);
    }
  }

  static async DownloadFile(fileUrl, filePath) {
    try {
      Logger.info('Downloading file', { fileUrl, filePath });

      const writer = fs.createWriteStream(filePath);

      const response = await Axios({
        url: fileUrl,
        method: 'GET',
        responseType: 'stream',
      });

      response.data.pipe(writer);

      return new Promise((resolve, reject) => {
        writer.on('finish', resolve);
        writer.on('error', reject);
      });
    } catch (e) {
      Logger.error(`Downloading file ${fileUrl}`, e);

      throw Response.createError(Message.tryAgain, e);
    }
  }

  static async GetVideoInfo(filePath) {
    try {
      Logger.info('Getting video info', { filePath });

      return await new Promise((resolve, reject) => {
        ffmpeg.ffprobe(filePath, (err, result) => {
          if (err) reject(err);
          else resolve(result);
        });
      });
    } catch (e) {
      Logger.error(`Getting video info: ${filePath}`, e);

      throw Response.createError(Message.tryAgain, e);
    }
  }

  static async GetYoutubeVideoInfo(youtubeUrl) {
    try {
      Logger.info('Getting youtube url info', { youtubeUrl });

      return await ytdl.getInfo(youtubeUrl);
    } catch (e) {
      Logger.error('Get youtube url info', e);

      throw Response.createError(Message.invalidYoutubeUrl, e);
    }
  }

  static async DownloadYoutubeVideo(youtubeUrl) {
    let filePath = null;
    let thumbnailPath = null;
    try {
      const info = await ytdl.getInfo(youtubeUrl);
      const format = ytdl.chooseFormat(info.formats, { filter: f => f.container === 'mp4', quality: 'highest' });

      const {
        videoDetails: {
          lengthSeconds,
          viewCount,
          averageRating,
          thumbnails,
          isFamilySafe,
          likes,
          dislikes,
          age_restricted: ageRestricted,
          videoId,
        },
      } = info;
      const { contentLength, hasVideo, hasAudio, qualityLabel, quality, container } = format;

      const { origin, pathname } = new url.URL(thumbnails[0].url);
      const thumbnail = `${origin}${pathname}`;

      const uuidStr = `${uuid()}-${+new Date()}`;
      filePath = `${ContentDir}/${uuidStr}.mp4`;
      thumbnailPath = `${ContentDir}/${uuidStr}-thumb${path.extname(thumbnail)}`;

      await ytdl.downloadFromInfo(info, { format }).pipe(fs.createWriteStream(filePath));
      await Helper.DownloadFile(thumbnail, thumbnailPath);

      const data = {
        filePath,
        thumbnailPath,
        lengthSeconds,
        isFamilySafe,
        viewCount,
        averageRating,
        likes,
        dislikes,
        ageRestricted,
        thumbnail,
        videoId,
        contentLength,
        hasVideo,
        hasAudio,
        qualityLabel,
        quality,
        container,
      };

      return data;
    } catch (e) {
      Helper.UnlinkFile(filePath);
      Helper.UnlinkFile(thumbnailPath);

      throw e;
    }
  }

  static FilterParam(param, allowedValues, defaultValues = null) {
    if (param) {
      param = _.intersection(param, allowedValues);
    }
    if (!param || !param.length) {
      param = defaultValues || allowedValues;
    }

    return param;
  }

  static Wait(seconds) {
    return new Promise(resolve => {
      setTimeout(resolve, seconds * 1000);
    });
  }

  static CreateLink(type, data) {
    const linkConfig = LinkType[type];
    data.type = type;
    const token = JwtToken.create(data, linkConfig.expiresIn);

    return `${ApiUrl}/${linkConfig.path}?${linkConfig.qKey}=${token}`;
  }
}

module.exports = Helper;
