const { Storage } = require('@google-cloud/storage');
const { ServiceAccountConfigFile, BucketName } = require('../config');
const Logger = require('./Logger');

// Creates a client
const storage = new Storage({ keyFilename: ServiceAccountConfigFile });

class StorageUtil {
  static upload(filePath, file, cache = false) {
    return storage.bucket(BucketName).upload(filePath, {
      // By setting the option `destination`, you can change the name of the
      // object you are uploading to a bucket.
      destination: file,
      metadata: {
        // Enable long-lived HTTP caching headers
        // Use only if the contents of the file will never change
        // (If the contents will change, use cacheControl: 'no-cache')
        cacheControl: cache ? 'public, max-age=31536000' : 'no-cache',
      },
    });
  }

  static download(file, filePath) {
    // Download a file from the bucket
    return storage
      .bucket(BucketName)
      .file(file)
      .download({
        // The path to which the file should be downloaded, e.g. "./file.txt"
        destination: filePath,
      });
  }

  /**
   *
   * @param {*} file
   * @param {*} expires seconds
   */
  static async signedUrl(file, expires = null) {
    // Get a v4 signed URL for reading the file
    const [url] = await storage
      .bucket(BucketName)
      .file(file)
      .getSignedUrl({
        version: 'v4',
        action: 'read',
        expires: Date.now() + (expires || 3600) * 1000, // default 60 minutes
      });

    return url;
  }

  static async get(prefix = null) {
    // Lists files in the bucket
    const [files] = await storage.bucket(BucketName).getFiles({
      prefix,
    });

    return files;
  }

  static async remove(file) {
    try {
      // Deletes the file from the bucket
      await storage
        .bucket(BucketName)
        .file(file)
        .delete();
    } catch (e) {
      Logger.error(`Storage: Deleting file ${file}`, e);
    }
  }
}

module.exports = StorageUtil;
