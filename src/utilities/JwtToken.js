const jwt = require('jsonwebtoken');

const { JwtSecret } = require('../config');
const Logger = require('./Logger');
const Message = require('./Message');
const Response = require('./Response');

class JwtToken {
  static create(data, expiresIn) {
    try {
      Logger.info('Creating jwt token');
      console.log(data, expiresIn);

      return jwt.sign(data, JwtSecret, { expiresIn });
    } catch (e) {
      Logger.error('Creating jwt token', e);

      throw Response.createError(Message.tryAgain, e);
    }
  }

  static verify(token, type = null) {
    try {
      Logger.info('Verifying jwt token');

      const tokenData = jwt.verify(token, JwtSecret);
      if (!tokenData.iat) {
        Logger.error('Iat not present in jwt token');

        throw Response.createError(Message.invalidLink);
      }

      if (type && type !== tokenData.type) {
        Logger.error('Jwt token type not matched');

        throw Response.createError(Message.invalidLink);
      }

      return tokenData;
    } catch (e) {
      Logger.error('Verifying jwt token', e);

      const errorType = JwtToken.convertError(e);
      throw Response.createError(Message[errorType], e);
    }
  }

  static convertError(error) {
    const errorMessages = {
      TokenExpiredError: 'tokenExpired',
      JsonWebTokenError: 'invalidLink',
    };

    return errorMessages[error.name] || 'tryAgain';
  }
}

module.exports = JwtToken;
