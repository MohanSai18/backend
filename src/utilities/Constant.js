module.exports.MaxPageLimit = 20;
module.exports.DefaultBoardId = 1;
module.exports.DefaultTopicName = 'Overview';
module.exports.DefaultAuthorName = 'Self';
module.exports.DefaultRating = 3;

module.exports.Role = {
  A: 1,
  S: 2,
};

module.exports.OrderType = {
  Asc: 'ASC',
  Desc: 'DESC',
};

module.exports.AddressEntity = {
  Institute: 'Institute',
  User: 'User',
};

module.exports.UserStatus = {
  Invited: 100,
  Active: 200,
  Inactive: 300,
};

module.exports.InstituteType = {
  School: 'School',
  Coaching: 'Coaching',
};

module.exports.InstituteStatus = {
  Active: 100,
  Inactive: 200,
};

module.exports.InstituteLeadStatus = {
  Pending: 100,
  Approved: 200,
  Rejected: 300,
};

module.exports.ContentType = {
  Pdf: 100,
  Video: 200,
};

module.exports.ContentStatus = {
  Saved: 100,
  Submitted: 200,
  Downloading: 300,
  Downloaded: 400,
  Uploading: 500,
  ApprovalPending: 600,
  Rejected: 700,
  Active: 800,
};

module.exports.ContentAccess = {
  InstituteOnly: 100,
  GlobalRequested: 200,
  Global: 300,
};

module.exports.ContentActivity = {
  Approve: 'Approve',
  Reject: 'Reject',
};

module.exports.QuestionStatus = {
  Active: 100,
  Inactive: 200,
};

module.exports.ContentOrderBy = {
  Rating: 'rating',
  CreatedAt: 'createdAt',
};

module.exports.LinkType = {
  InstituteInvite: {
    name: 'InstituteInvite',
    path: 'public/users/institute-invite',
    qKey: 't',
    expiresIn: 3600 * 24,
  },
};

module.exports.jwtConfig = {
  expiresIn: 3600 * 24,
}
