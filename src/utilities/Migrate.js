const cmd = require('node-cmd');

class Migrate {
  static async init() {
    return new Promise((resolve, reject) => {
      if (process.env.AUTO_MIGRATE && process.env.AUTO_MIGRATE.toLowerCase() === 'true') {
        if (process.env.NODE_ENV) {
          cmd.get('sequelize-cli db:migrate', (err, data, stderr) => {
            if (err) {
              reject(err);
            } else if (stderr) {
              reject(stderr);
            } else {
              resolve(data);
            }
          });
        } else {
          reject(Error('NODE_ENV not set'));
        }
      }
    });
  }
}

module.exports = Migrate;
