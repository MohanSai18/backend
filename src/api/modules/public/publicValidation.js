const { celebrate, Joi } = require('celebrate');
const { Validate } = require('../../middlewares');

module.exports = {
  login: celebrate({
    body: Joi.object().keys({
      email: Validate.emailReq,
      password: Validate.passwordReq,
    }),
  })
};
