const { Router } = require('express');
const {ToData} = require('../../middlewares');
const controller = require('./publicController');
const validation = require('./publicValidation');

const router = Router({ mergeParams: true });

router.post('/login', validation.login, ToData, controller.login );

module.exports = router;