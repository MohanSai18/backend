const { UserService } = require('../../../services');
const { Response, JwtToken } = require('../../../utilities');
const { jwtConfig } = require('../../../utilities/Constant');


class publicController {
    static async login(req, res) {
        try {
          const srvRes = await UserService.getOneByEmail(req.data.params);

          const token = JwtToken.create({userId: srvRes.data.userId, roleId: srvRes.data.roleId}, jwtConfig.expiresIn);
    
          Response.success(res, false, {fname: srvRes.data.fname, lname: srvRes.data.lname, roleId: srvRes.data.roleId, token: token});
        } catch (e) {
          Response.fail(res, e);
        }
      }
}

module.exports = publicController;