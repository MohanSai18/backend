const { Router } = require('express');

const { ToData } = require('../../middlewares');
const controller = require('./testController');
const validation = require('./testValidation');

const router = Router({ mergeParams: true });

router.post('/admin', validation.createAdmin, ToData, controller.createAdmin);
router.post('/login', validation.login, ToData, controller.login);
router.get('/migrate/users', ToData, controller.migrateUsers);
router.get('/migrate/institute-id', ToData, controller.migrateUserInstituteId);

module.exports = router;
