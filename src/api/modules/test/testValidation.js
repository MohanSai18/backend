const { celebrate, Joi } = require('celebrate');
const { Validate } = require('../../middlewares');

module.exports = {
  createAdmin: celebrate({
    body: Joi.object().keys({
      fname: Validate.stringReq,
      lname: Validate.lname,
      email: Validate.emailReq,
      password: Validate.passwordReq,
    }),
  }),
  login: celebrate({
    body: Joi.object().keys({
      email: Validate.emailReq,
      password: Validate.passwordReq,
    }),
  }),
};
