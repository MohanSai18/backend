/* eslint-disable no-await-in-loop */
const Sequelize = require('sequelize');
const { Models } = require('../../../loaders/sequelize');
const Logger = require('../../../utilities/Logger');
const { TestService, FirebaseUserService } = require('../../../services');
const {
  Response,
  Constant: { Role },
} = require('../../../utilities');

class TestController {
  static async createAdmin(req, res) {
    try {
      const srvRes = await TestService.createAdmin(req.data);

      Response.success(res, srvRes);
    } catch (e) {
      Response.fail(res, e);
    }
  }

  static async login(req, res) {
    try {
      const srvRes = await TestService.login(req.data);

      Response.success(res, srvRes);
    } catch (e) {
      Response.fail(res, e);
    }
  }

  /**
   *
   * @param {*} req
   * @param {*} res
   * @description to update uid in db and userId in customClaims in firebase
   */
  static async migrateUsers(req, res) {
    try {
      Logger.info('getting users from database');
      const dbUsers = await Models.user.findAll({
        where: { uid: { [Sequelize.Op.is]: null } },
      });

      for (const user of dbUsers) {
        Logger.info('getting user from firebase', { id: user.id });
        await FirebaseUserService.get(user.id.toString())
          .then(async fbRes => {
            Logger.info('updating custom claims', { id: user.id });
            await FirebaseUserService.setCustomAttributes(user.id.toString(), {
              userId: user.id,
              roleIds: fbRes.data.customClaims.roleIds,
            });

            Logger.info('updating user in database', { id: user.id });
            user.uid = fbRes.data.uid;
            await user.save();
          })
          .catch(e => {
            Logger.error('error', e);
          });
      }

      Response.success(res);
    } catch (e) {
      Response.fail(res, e);
    }
  }

  /**
   *
   * @param {*} req
   * @param {*} res
   * @description to update institute in in customClaims in firebase
   */
  static async migrateUserInstituteId(req, res) {
    try {
      Logger.info('getting users from database');

      const institutes = await Models.institute.findAll({
        attributes: ['id'],
        include: [
          {
            model: Models.user,
            through: { attributes: [] },
            attributes: ['id', 'uid'],
          },
        ],
      });

      for (const institute of institutes) {
        for (const user of institute.users) {
          Logger.info('getting user from firebase', { id: user.id });

          await FirebaseUserService.get(user.uid)
            .then(async fbRes => {
              Logger.info('custom claims', { id: user.id, customClaims: fbRes.data.customClaims });
              if (!fbRes.data.customClaims.instituteId) {
                Logger.info('updating custom claims', { id: user.id });

                fbRes.data.customClaims.instituteId = institute.id;

                await FirebaseUserService.setCustomAttributes(user.uid, fbRes.data.customClaims);
              }
            })
            .catch(e => {
              Logger.error('error', e);
            });
        }
      }

      Response.success(res);
    } catch (e) {
      Response.fail(res, e);
    }
  }
}

module.exports = TestController;
