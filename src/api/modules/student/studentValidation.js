const { celebrate, Joi } = require('celebrate');
const { Validate } = require('../../middlewares');

module.exports = {
  create: celebrate({
    body: Joi.object().keys({
      fname: Validate.stringReq,
      lname: Validate.lname,
      email: Validate.emailReq,
      dob: Validate.string,
      address: Validate.lname,
    }),
  }),
  update: celebrate({
    params: Joi.object().keys({
      studentId: Validate.idReq,
    }),
    body: Joi.object().keys({
      fname: Validate.stringReq,
      lname: Validate.lname,
      email: Validate.emailReq,
      dob: Validate.string,
      address: Validate.lname,
    }),
  }),
  getOne: celebrate({
    params: Joi.object().keys({
      studentId: Validate.idReq,
    }),
  }),
  remove: celebrate({
    params: Joi.object().keys({
      studentId: Validate.idReq,
    }),
  }),
};
