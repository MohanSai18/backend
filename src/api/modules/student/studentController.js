const { StudentService } = require('../../../services');
const { Response } = require('../../../utilities');

class StudentController {
  static async create(req, res) {
    try {
      const srvRes = await StudentService.create(req.data);

      Response.success(res, srvRes);
    } catch (e) {
      Response.fail(res, e);
    }
  }

  static async update(req, res) {
    try {
      const srvRes = await StudentService.update(req.data);

      Response.success(res, srvRes);
    } catch (e) {
      Response.fail(res, e);
    }
  }

  static async getAll(req, res) {
    try {
      const srvRes = await StudentService.getAll(req.data);

      Response.success(res, srvRes);
    } catch (e) {
      Response.fail(res, e);
    }
  }

  static async getOne(req, res) {
    try {
      const srvRes = await StudentService.getOne(req.data);

      Response.success(res, srvRes);
    } catch (e) {
      Response.fail(res, e);
    }
  }

  static async remove(req, res) {
    try {
      const srvRes = await StudentService.remove(req.data);

      Response.success(res, srvRes);
    } catch (e) {
      Response.fail(res, e);
    }
  }
}

module.exports = StudentController;
