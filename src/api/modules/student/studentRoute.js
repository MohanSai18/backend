const { Router } = require('express');

const { IsAllowed, ToData } = require('../../middlewares');
const {
  Constant: { Role },
} = require('../../../utilities');
const controller = require('./studentController');
const validation = require('./studentValidation');

const router = Router({ mergeParams: true });

router.post('', IsAllowed([Role.A]), validation.create, ToData, controller.create);
router.get('', IsAllowed([Role.A]), ToData, controller.getAll);
router.get('/:studentId', IsAllowed([Role.A, Role.S]), validation.getOne, ToData, controller.getOne);
router.put('/:studentId', IsAllowed([Role.A, Role.S]), validation.update, ToData, controller.update);
router.delete('/:studentId', IsAllowed([Role.A]), validation.remove, ToData, controller.remove);

module.exports = router;
