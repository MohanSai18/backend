const { AuthorService } = require('../../../services');
const { Response } = require('../../../utilities');

class AuthorController {
  static async create(req, res) {
    try {
      const srvRes = await AuthorService.create(req.data);

      Response.success(res, srvRes);
    } catch (e) {
      Response.fail(res, e);
    }
  }

  static async update(req, res) {
    try {
      const srvRes = await AuthorService.update(req.data);

      Response.success(res, srvRes);
    } catch (e) {
      Response.fail(res, e);
    }
  }

  static async getAll(req, res) {
    try {
      const srvRes = await AuthorService.getAll(req.data);

      Response.success(res, srvRes);
    } catch (e) {
      Response.fail(res, e);
    }
  }

  static async getOne(req, res) {
    try {
      const srvRes = await AuthorService.getOne(req.data);

      Response.success(res, srvRes);
    } catch (e) {
      Response.fail(res, e);
    }
  }

  static async remove(req, res) {
    try {
      const srvRes = await AuthorService.remove(req.data);

      Response.success(res, srvRes);
    } catch (e) {
      Response.fail(res, e);
    }
  }
}

module.exports = AuthorController;
