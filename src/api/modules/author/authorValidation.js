const { celebrate, Joi } = require('celebrate');
const { Validate } = require('../../middlewares');

module.exports = {
  create: celebrate({
    body: Joi.object().keys({
      name: Validate.stringReq,
      description: Validate.textReq,
    }),
  }),
  update: celebrate({
    params: Joi.object().keys({
      authorId: Validate.idReq,
    }),
    body: Joi.object().keys({
      name: Validate.stringReq,
      description: Validate.textReq,
    }),
  }),
  getOne: celebrate({
    params: Joi.object().keys({
      authorId: Validate.idReq,
    }),
  }),
  remove: celebrate({
    params: Joi.object().keys({
      authorId: Validate.idReq,
    }),
  }),
};
