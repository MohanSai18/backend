const { Router } = require('express');

const { IsAllowed, ToData } = require('../../middlewares');
const {
  Constant: { Role },
} = require('../../../utilities');
const controller = require('./authorController');
const validation = require('./authorValidation');

const router = Router({ mergeParams: true });

router.post('', IsAllowed([Role.A, Role.ST]), validation.create, ToData, controller.create);
router.get('', IsAllowed([Role.A, Role.ST, Role.SR, Role.IRV, Role.IT, Role.T]), ToData, controller.getAll);
router.get('/:authorId', IsAllowed([Role.A, Role.ST]), validation.getOne, ToData, controller.getOne);
router.put('/:authorId', IsAllowed([Role.A, Role.ST]), validation.update, ToData, controller.update);
router.delete('/:authorId', IsAllowed([Role.A, Role.ST]), validation.remove, ToData, controller.remove);

module.exports = router;
