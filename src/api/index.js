/* eslint-disable global-require */
const { Response, Logger } = require('../utilities');
const { Me, SetAuthUser, SetData, SetAutoFill, Token } = require('./middlewares');
const { IsLocal } = require('../config');

exports.loadRoutes = app => {
  app.use('*', (req, res, next) => {
    res.set({
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET, POST, PUT, PATCH, DELETE, OPTIONS',
      'Access-Control-Allow-Headers': 'Content-Type, Authorization',
    });
    if (req.method === 'OPTIONS') {
      res.status(200).json({ status: 'Okay' });
    } else {
      next();
    }
  });

  app.all('/status', (req, res) => {
    Logger.info('checking status', { status: 1 });

    Response.success(res, {
      data: {
        headers: req.headers,
        params: req.params,
        query: req.query,
        body: req.body,
      },
    });
  });

  app.use(SetData);
  app.use(SetAutoFill);

  /**
   * Test routes
   */
  if (IsLocal) {
    app.use('/test', require('./modules/test/testRoute'));
  }

  /**
   * Public routes
   */
  app.use('/public', require('./modules/public/publicRoute'));

  /**
   * Token verification
   */
  app.use(Token);

  app.use(SetAuthUser);

  /**
   * Private routes
   */
  // app.use('/me', Me, require('./modules/authUser'));
  app.use('/authors', require('./modules/author/authorRoute'));
  app.use('/students', require('./modules/student/studentRoute'));
};
