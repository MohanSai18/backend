/* eslint-disable no-await-in-loop */
const multer = require('multer');
const _ = require('lodash');
const path = require('path');
const fs = require('fs');
const os = require('os');
const FileType = require('file-type');
const { Response, Logger, Message } = require('../../utilities');

const upload = multer({ dest: os.tmpdir() });

const addFileExt = async file => {
  const path1 = file.path;
  const ext = path.extname(file.originalname);
  file.filename += ext;
  file.path += ext;
  await fs.renameSync(path1, file.path);
};

const checkMimeType = async file => {
  const exts = ['jpeg', 'jpg'];
  const fileExt = path.extname(file.originalname).substring(1);
  const fileExts = exts.includes(fileExt) ? exts : [fileExt];
  const fileType = await FileType.fromFile(file.path);
  if (!fileType || !fileExts.includes(fileType.ext) || fileType.mime !== file.mimetype) {
    Logger.info('File upload error', { file, fileType });

    throw Response.createError(Message.invalidFile);
  }
};

class MulterMiddleware {
  static single(name) {
    const middleware = upload.single(name);

    return (req, res, next) =>
      middleware(req, res, async result => {
        try {
          if (_.isObjectLike(req.file)) {
            await checkMimeType(req.file);
            await addFileExt(req.file);

            req.body[name] = req.file;
          }
          next(result);
        } catch (e) {
          next(e);
        }
      });
  }

  static array(name, maxCount) {
    const middleware = upload.array(name, maxCount);

    return (req, res, next) =>
      middleware(req, res, async result => {
        try {
          if (req.files.length) {
            req.files = await Promise.all(
              req.files.map(async file => {
                await checkMimeType(file);
                await addFileExt(file);

                return file;
              }),
            );

            req.body[name] = req.files;
          }
          next(result);
        } catch (e) {
          next(e);
        }
      });
  }

  static fields(fields = []) {
    const middleware = upload.fields(fields);

    return (req, res, next) =>
      middleware(req, res, async result => {
        try {
          for (const field of fields) {
            if (field.name in req.files) {
              for (const file of req.files[field.name]) {
                await checkMimeType(file);
                await addFileExt(file);
              }

              if (field.maxCount && field.maxCount === 1) {
                req.body[field.name] = req.files[field.name][0];
              } else {
                req.body[field.name] = req.files[field.name];
              }
            }
          }
          next(result);
        } catch (e) {
          next(e);
        }
      });
  }
}

module.exports = MulterMiddleware;
