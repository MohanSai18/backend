const {
  Constant: { InstituteType, Role },
} = require('../../utilities');

const AutoFill = {
  '/admin': {
    roleId: Role.A,
  },
  '/students': {
    roleId: Role.S,
  },
};

module.exports = (req, res, next) => {
  // eslint-disable-next-line guard-for-in
  for (const url in AutoFill) {
    const urlWithSlash = `${url}/`;
    if (
      req._parsedUrl.pathname.startsWith(urlWithSlash) ||
      (req._parsedUrl.pathname.length === url.length && req._parsedUrl.pathname === url)
    ) {
      Object.assign(req.data.autoFilled, AutoFill[url]);
      break;
    }
  }

  next();
};
