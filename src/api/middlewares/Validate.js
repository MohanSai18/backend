const { Joi } = require('celebrate');

const {
  Constant: { MaxPageLimit, OrderType },
} = require('../../utilities');

const string = Joi.string()
  .trim()
  .max(255);
const stringReq = string.required();
const text = Joi.string()
  .trim()
  .max(65535);
const textReq = string.required();
const number = Joi.number();
const numberReq = number.required();
const posNumber = number.positive().min(1);
const posNumberReq = posNumber.required();
const integer = number.integer();
const integerReq = integer.required();
const posInteger = posNumber.integer();
const posIntegerReq = posInteger.required();
const posIntegers = Joi.array()
  .items(posInteger)
  .unique();
const posIntegersReq = Joi.array()
  .items(posIntegerReq)
  .unique()
  .required();
const email = string.email();
const emailReq = email.required();
const uri = string.uri();
const uriReq = uri.required();
const phone = string
  .regex(/^[6-9]{1}[0-9]{9}$/, 'mobile')
  .error(() => '10 digit mobile number (in string format) required');
const phoneReq = phone.required();
const password = string.min(8).max(20);
const passwordReq = password.required();
const lname = string.allow('', null).default(null);

module.exports = {
  file: Joi.object().keys({
    fieldname: stringReq,
    originalname: stringReq,
    encoding: stringReq,
    mimetype: stringReq,
    size: posNumberReq,
    destination: stringReq,
    filename: stringReq,
    path: stringReq,
  }),
  videoFile: Joi.object().keys({
    fieldname: stringReq,
    originalname: stringReq,
    encoding: stringReq,
    mimetype: string
      .regex(/video\/.*/)
      .error(() => 'Video file required')
      .required(),
    size: posNumberReq,
    destination: stringReq,
    filename: stringReq,
    path: stringReq,
  }),
  pdfFile: Joi.object().keys({
    fieldname: stringReq,
    originalname: stringReq,
    encoding: stringReq,
    mimetype: string
      .valid('application/pdf')
      .error(() => 'Pdf file required')
      .required(),
    size: posNumberReq,
    destination: stringReq,
    filename: stringReq,
    path: stringReq,
  }),
  imageFile: Joi.object().keys({
    fieldname: stringReq,
    originalname: stringReq,
    encoding: stringReq,
    mimetype: string
      .regex(/image\/.*/)
      .error(() => 'Image file required')
      .required(),
    size: posNumberReq,
    destination: stringReq,
    filename: stringReq,
    path: stringReq,
  }),
  id: posInteger,
  idReq: posIntegerReq,
  ids: posIntegers,
  idsReq: posIntegersReq,
  phone,
  phoneReq,
  password,
  passwordReq,
  pageLimit: posInteger.max(MaxPageLimit).default(MaxPageLimit),
  page: posInteger.default(1),
  rating: posInteger.max(5),
  orderType: string.valid(Object.values(OrderType)),
  string,
  stringReq,
  text,
  textReq,
  number,
  numberReq,
  posNumber,
  posNumberReq,
  integer,
  integerReq,
  posInteger,
  posIntegerReq,
  posIntegers,
  posIntegersReq,
  email,
  emailReq,
  uri,
  uriReq,
  lname,
};
