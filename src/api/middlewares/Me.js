module.exports = (req, res, next) => {
  req.data.params.me = true;

  next();
};
