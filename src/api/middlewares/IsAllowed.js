const _ = require('lodash');
const { Logger, Message, Response } = require('../../utilities');

module.exports = roleIds => (req, res, next) => {
  if (_.indexOf(roleIds, req.data.authUser.roleId) !== -1) {
    next();
  } else {
    const extra = {
      userId: req.data.authUser && req.data.authUser.userId,
      roleId: req.data.authUser && req.data.authUser.roleId,
      method: req.method,
      baseUrl: req.baseUrl,
      originalUrl: req.originalUrl,
      params: req.params,
      routePath: req.route.path,
    };
    Logger.error('Role not matching with allowed roles in api.', extra);

    const errRes = Response.createError(Message.noPermission);
    errRes.extra = extra;
    Response.fail(res, errRes);
  }
};
