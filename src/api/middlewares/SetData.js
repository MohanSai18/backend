module.exports = (req, res, next) => {
  req.data = {
    params: {},
    autoFilled: {
      createdById: null,
      updatedById: null,
    },
    authUser: null,
  };

  next();
};
