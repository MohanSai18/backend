const { celebrate, Joi } = require('celebrate');

module.exports = celebrate({
  headers: Joi.object()
    .keys({
      authorization: Joi.string()
        .trim()
        .required(),
    })
    .unknown(),
});
