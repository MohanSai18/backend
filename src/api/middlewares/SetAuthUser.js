const { Logger, Message, Response, JwtToken } = require('../../utilities');

module.exports = async (req, res, next) => {
  try {
    if (req.headers.authorization) {
      const authorization = req.headers.authorization.trim();
      if (authorization.startsWith('Bearer ')) {
        const idToken = authorization.substring(7);
        if (idToken) {
          Logger.info('Jwt: Validating id token');

          const authUser = await JwtToken.verify(idToken);
          if (!authUser.roleId) {
            Logger.error('Anonymous user. Role not found');

            throw Response.createError(Message.noPermission);
          }

          req.data.authUser = authUser;
          req.data.autoFilled.createdById = authUser.userId;
          req.data.autoFilled.updatedById = authUser.userId;
        }
      } else {
        Logger.error('Invalid authorization value');

        throw Response.createError(Message.invalidToken);
      }
    }

    next();
  } catch (e) {
    Logger.error('Jwt: Validating id token', e);

    const errRes = Response.createError(Message.tryAgain, e);
    Response.fail(res, errRes);
  }
};
