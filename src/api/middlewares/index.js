/* eslint-disable global-require */
module.exports = {
  IsAllowed: require('./IsAllowed'),
  Me: require('./Me'),
  Multer: require('./Multer'),
  SetAuthUser: require('./SetAuthUser'),
  SetAutoFill: require('./SetAutoFill'),
  SetData: require('./SetData'),
  ToData: require('./ToData'),
  Token: require('./Token'),
  Validate: require('./Validate'),
};
