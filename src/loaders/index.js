require('./lodash');
const { Logger, Migrate } = require('../utilities');
const expressLoader = require('./express');
const { sequelize } = require('./sequelize');
// const schedule = require('./schedule');

const loader = async function({ expressApp }) {
  await sequelize.authenticate();
  Logger.info('✌️ DB loaded and connected!');

  await Migrate.init();
  Logger.info('✌️ Migration ran!');

  // await schedule.init();

  await expressLoader.loadModules({ app: expressApp });
  Logger.info('✌️ Express loaded');
};

module.exports = loader;
