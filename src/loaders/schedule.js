const findRemoveSync = require('find-remove');
const Sequelize = require('sequelize');
const moment = require('moment');
const { ContentDir, InstanceId } = require('../config');
const { Models, sequelize } = require('./sequelize');
const { ContentService, EmailService, FirebaseUserService } = require('../services');
const {
  Helper,
  Logger,
  Constant: { ContentStatus, UserStatus },
} = require('../utilities');

class Schedule {
  static async init() {
    // TODO distribute schedules in instances if running multiple
    if (InstanceId === '0') {
      Schedule.downloadYoutubeVideos(60, 5);
      Schedule.uploadContents(60, 5);
      Schedule.deleteIncompleteDbContents(3600, 3600 * 24 * 10);
      Schedule.deleteOldFiles(300, 3600, ContentDir);
      Schedule.createFirebaseUsers(60);
    }
  }

  /**
   *
   * @param {*} interval
   * @param {*} fileAge
   * @description deletes temporary files older than 10 hours every hour
   */
  static deleteOldFiles(interval, fileAge, dir) {
    setInterval(() => {
      Logger.info('Schedule: Deleting old files', { dir });

      const result = findRemoveSync(dir, { age: { seconds: fileAge }, files: '*.*', ignore: '.gitkeep' });

      Logger.info('Schedule: Deleted old files', { dir, count: Object.values(result).length });
    }, 1000 * interval);
  }

  /**
   *
   * @param {*} interval
   * @param {*} noOfFiles
   * @description download youtube videos
   */
  static downloadYoutubeVideos(interval, noOfFiles) {
    setInterval(async () => {
      try {
        Logger.info('Schedule: Download youtube videos');

        const contents = await Models.content.findAll({
          where: {
            [Sequelize.Op.or]: [
              {
                statusId: ContentStatus.Submitted,
              },
              {
                statusId: ContentStatus.Downloading,
                updatedAt: {
                  [Sequelize.Op.lt]: moment().subtract(6, 'hours'),
                },
              },
            ],
          },
          limit: noOfFiles,
          order: [['id', 'asc']],
        });

        for (const content of contents) {
          ContentService.downloadYoutubeVideo(content);
        }
      } catch (e) {
        Logger.error('Schedule: Download youtube videos', e);
      }
    }, 1000 * interval);
  }

  /**
   *
   * @param {*} interval
   * @param {*} noOfFiles
   * @description upload files to storage
   */
  static uploadContents(interval, noOfFiles) {
    setInterval(async () => {
      try {
        Logger.info('Schedule: Upload files');

        const contents = await Models.content.findAll({
          where: {
            [Sequelize.Op.or]: [
              {
                statusId: ContentStatus.Downloaded,
              },
              {
                statusId: ContentStatus.Uploading,
                updatedAt: {
                  [Sequelize.Op.lt]: moment().subtract(6, 'hours'),
                },
              },
            ],
          },
          include: [
            {
              model: Models.user,
              as: 'createdBy',
              attributes: ['id'],
              include: [
                {
                  model: Models.role,
                  attributes: ['id'],
                  through: { attributes: [] },
                },
              ],
            },
            {
              model: Models.institute,
              as: 'institute',
              attributes: ['autoApproveContent'],
            },
          ],
          limit: noOfFiles,
          order: [['id', 'asc']],
        });

        for (const content of contents) {
          ContentService.upload(content);
        }
      } catch (e) {
        Logger.error('Schedule: Upload files', e);
      }
    }, 1000 * interval);
  }

  /**
   *
   * @param {*} interval
   * @param {*} contentAge
   * @description Delete incomplete db content
   */
  static deleteIncompleteDbContents(interval, contentAge) {
    setInterval(async () => {
      try {
        Logger.info('Schedule: Delete incomplete db content');

        const deletedCount = await Models.content.destroy({
          where: {
            statusId: ContentStatus.Saved,
            createdAt: {
              [Sequelize.Op.lt]: moment().subtract(contentAge, 'seconds'),
            },
          },
        });

        Logger.info('Schedule: Delete incomplete db content', { deletedCount });
      } catch (e) {
        Logger.error('Schedule: Delete incomplete db content', e);
      }
    }, 1000 * interval);
  }

  /**
   *
   * @param {*} minInterval
   * @description Create users in firebase
   */
  static async createFirebaseUsers(minInterval) {
    try {
      Logger.info('Schedule: Create firebase users');

      // eslint-disable-next-line no-constant-condition
      while (1) {
        const startTime = moment().unix();
        try {
          // eslint-disable-next-line no-await-in-loop
          const users = await Models.user.findAll({
            attributes: ['id', 'fname', 'lname', 'email'],
            where: {
              uid: {
                [Sequelize.Op.is]: null,
              },
              statusId: UserStatus.Active,
            },
            include: [
              {
                model: Models.role,
                attributes: ['id'],
                through: { attributes: [] },
                required: true,
              },
              {
                model: Models.institute,
                attributes: ['id'],
                through: { attributes: [] },
              },
            ],
            order: [['id', 'asc']],
            limit: 1000,
          });

          for (const user of users) {
            const userJson = user.toJSON();
            userJson.roleId = user.roles.map(r => r.id);
            if (userJson.institutes.length) {
              userJson.instituteId = userJson.institutes[0].id;
            }

            // eslint-disable-next-line no-await-in-loop
            await sequelize.transaction(async transaction => {
              const { data: fbUser } = await FirebaseUserService.create(userJson);

              user.uid = fbUser.uid;
              await user.save({ transaction });
            });

            EmailService.userCreate(user);
          }
        } catch (e) {
          Logger.error('Schedule: Create firebase users', e);
        }

        const intervalRemaining = minInterval - (moment().unix() - startTime);
        if (intervalRemaining > 0) {
          // eslint-disable-next-line no-await-in-loop
          await Helper.Wait(intervalRemaining);
        }
      }
    } catch (e) {
      Logger.error('Schedule: Create firebase users', e);
    }
  }
}

module.exports = Schedule;
