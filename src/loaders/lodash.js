const _ = require('lodash');

// added custom method
/* const testData = [
  {
    id: '1',
    val: 'v1',
  },
  {
    id: '2',
    val: 'v2',
  },
]; */

// output: [ '1', '2' ]

// output: { v1: '1', v2: '2' }

/* output: {
  v1: {
    id: '1',
    val: 'v1',
  },
  v2: {
    id: '2',
    val: 'v2',
  },
}; */

_.mixin({
  pluck: (list, valueKey, indexKey) => {
    if (indexKey) {
      const result = {};
      for (const v of list) {
        result[v[indexKey]] = valueKey ? v[valueKey] : v;
      }

      return result;
    }

    return list.map(v => v[valueKey]);
  },
});
_.mixin({
  toFixed: (value, decimalPoint = 2) => parseFloat(value.toFixed(decimalPoint)),
});
