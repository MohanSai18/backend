module.exports = (queryInterface, DataTypes) => {
  const model = queryInterface.define(
    'role',
    {
      roleId: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      name: {
        type: DataTypes.STRING,
      },
    },
    {
      timestamps: false,
    },
  );

  model.associate = models => {
    model.hasMany(models.user, { foreignKey: 'roleId', as: 'UR' });
  };

  return model;
};
