const _ = require('lodash');

module.exports = (queryInterface, DataTypes) => {
    const model = queryInterface.define(
      'courseMembers',
      {
        courseMemberId: {
          type: DataTypes.INTEGER,
          autoIncrement: true,
          primaryKey: true,
        },
        userId: {
            type: DataTypes.INTEGER,
            allowNull: false,
          },
        courseId: {
          type: DataTypes.INTEGER,
          allowNull: false,
        },
        registrationId: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        installments: {
          type: DataTypes.INTEGER,
          allowNull: false,
        },
        paidAmount: {
            type: DataTypes.DECIMAL(10,2),
            allowNull: false,
        }
      },
      {
        paranoid: true,
        defaultScope: {
          attributes: { exclude: ['deletedAt'] },
        },
      },
    );

    model.autoFilledColumns = [];
    model.createColumns = _.difference(Object.keys(model.tableAttributes), [
      ...Object.keys(model._timestampAttributes),
      ...model.primaryKeyAttributes,
    ]);
    model.updateColumns = _.without(model.createColumns, []);
  
    model.associate = models => {
      model.belongsTo(models.user, { foreignKey: 'userId', as: 'UCM' });
      model.belongsTo(models.course, { foreignKey: 'courseId', as: 'CCM' });
    };
  
    return model;
  };
  