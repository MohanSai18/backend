const _ = require('lodash');

module.exports = (queryInterface, DataTypes) => {
  const model = queryInterface.define(
    'user',
    {
      userId: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      fname: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      lname: {
        type: DataTypes.STRING,
        defaultValue: null,
      },
      email: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      dob: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      address: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      roleId: {
        type: DataTypes.INTEGER,
        allowNull: false,
      }
    },
    {
      paranoid: true,
      defaultScope: {
        attributes: { exclude: ['deletedAt'] },
      },
    },
  );

  model.autoFilledColumns = [];
  model.createColumns = _.difference(Object.keys(model.tableAttributes), [
    ...Object.keys(model._timestampAttributes),
    ...model.primaryKeyAttributes,
  ]);
  model.updateColumns = _.without(model.createColumns, []);

  model.associate = models => {
    model.hasMany(models.courseMembers, { foreignKey: 'userId', as: 'UCM' });
    model.belongsTo(models.role, { foreignKey: 'roleId', as: 'UR' });
  };

  return model;
};
