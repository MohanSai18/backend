const _ = require('lodash');

module.exports = (queryInterface, DataTypes) => {
    const model = queryInterface.define(
      'course',
      {
        courseId: {
          type: DataTypes.INTEGER,
          autoIncrement: true,
          primaryKey: true,
        },
        name: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        fee: {
            type: DataTypes.DECIMAL(10,2),
            allowNull: false,
        }
      },
      {
        paranoid: true,
        defaultScope: {
          attributes: { exclude: ['deletedAt'] },
        },
      },
    );

    model.autoFilledColumns = [];
    model.createColumns = _.difference(Object.keys(model.tableAttributes), [
      ...Object.keys(model._timestampAttributes),
      ...model.primaryKeyAttributes,
    ]);
    model.updateColumns = _.without(model.createColumns, []);

  
    model.associate = models => {
      model.hasMany(models.courseMembers, { foreignKey: 'courseId', as: 'CCM' });
    };
  
    return model;
  };
  