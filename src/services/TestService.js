// eslint-disable-next-line import/no-extraneous-dependencies
const firebase = require('firebase');
const UserService = require('./UserService');
const {
  Logger,
  Message,
  Response,
  Constant: { Role },
} = require('../utilities');

class TestService {
  static async createAdmin({ params, autoFilled }) {
    try {
      autoFilled.roleId = Role.A;
      autoFilled.createdById = 1;
      autoFilled.updatedById = 1;
      const user = await UserService.create({ params, autoFilled });

      return { data: user };
    } catch (e) {
      Logger.error('Error firebase login', e);

      throw Response.createError(Message.tryAgain, e);
    }
  }

  static async login({ params }) {
    try {
      if (!firebase.apps.length) {
        throw Response.createError(Message.noFirebaseClient);
      }
      const user = await firebase.auth().signInWithEmailAndPassword(params.email, params.password);

      return { data: user };
    } catch (e) {
      Logger.error('Error firebase login', e);

      throw Response.createError(Message.tryAgain, e);
    }
  }
}

module.exports = TestService;
