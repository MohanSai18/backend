const { Models, sequelize } = require('../loaders/sequelize');
const {
  Response,
  Logger,
  Message,
  Helper: { BuildCreate, BuildUpdate },
} = require('../utilities');

class AuthorService {
  static async create({ params, autoFilled }, transaction = null) {
    try {
      Logger.info('Creating author');

      const insertData = BuildCreate(Models.author, params, autoFilled);
      const author = await Models.author.create(insertData, { transaction });

      return {
        data: author,
        message: 'Author created',
      };
    } catch (e) {
      Logger.error('Creating author', e);

      throw Response.createError(Message.tryAgain, e);
    }
  }

  static async update({ params, autoFilled, authUser }, transaction = null) {
    try {
      Logger.info('Updating author');

      const { data: author } = await AuthorService.getOne({ params, autoFilled, authUser });

      const updateData = BuildUpdate(Models.author, params, autoFilled);
      Object.assign(author, updateData);
      await author.save({ transaction });

      return {
        data: author,
        message: 'Author updated',
      };
    } catch (e) {
      Logger.error('Updating author', e);

      throw Response.createError(Message.tryAgain, e);
    }
  }

  static async getAll() {
    try {
      Logger.info('Getting authors');

      const authors = await Models.author.findAll();

      return { data: authors };
    } catch (e) {
      Logger.error('Getting authors', e);

      throw Response.createError(Message.tryAgain, e);
    }
  }

  static async getOne({ params }) {
    try {
      Logger.info('Getting author');

      const author = await Models.author.findOne({
        where: {
          id: params.authorId,
        },
      });

      if (!author) {
        throw Response.createError(Message.noAuthor);
      }

      return {
        data: author,
      };
    } catch (e) {
      Logger.error('Getting author', e);

      throw Response.createError(Message.tryAgain, e);
    }
  }

  static async remove({ params, autoFilled, authUser }, transaction = null) {
    try {
      Logger.info('Deleting author');

      const { data: author } = await AuthorService.getOne({ params, autoFilled, authUser });

      await sequelize.transaction(async t => {
        transaction = transaction || t;

        await author.destroy({ transaction });
        author.updatedById = authUser.userId;
        await author.save({ paranoid: false, transaction });
      });

      return {
        message: 'Author deleted',
      };
    } catch (e) {
      Logger.error('Deleting author', e);

      throw Response.createError(Message.tryAgain, e);
    }
  }
}

module.exports = AuthorService;
