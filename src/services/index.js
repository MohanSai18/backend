const AuthorService = require('./AuthorService');
const TestService = require('./TestService');
const UserService = require('./UserService');
const StudentService = require('./StudentService');

const services = {
    AuthorService,
    TestService,
    UserService,
    StudentService,
}

module.exports = services;