const Sequelize = require('sequelize');
const { Models, sequelize } = require('../loaders/sequelize');
const { BucketDir } = require('../config');
// const EmailService = require('./EmailService');
// const FirebaseUserService = require('./FirebaseUserService');
const {
  Logger,
  Message,
  Response,
  Storage,
  Helper: { BuildCreate, BuildUpdate, UnlinkFile, GeneratePassword },
  Constant: { UserStatus, Role },
} = require('../utilities');

class StudentService {
  static async create({ params, autoFilled, authUser }, transaction = null) {
    try {
      Logger.info('Creating user');

      const dbData = await Models.user.findOne({ where: { email: params.email } });
      if (dbData) {
        throw Response.createError(Message.emailAlreadyExist);
      }

      const user = await sequelize.transaction(async t => {
        transaction = transaction || t;

        params.password = GeneratePassword();
        
        const insertUserData = BuildCreate(Models.user, params, autoFilled);
        const newUser = await Models.user.create(insertUserData, { transaction });
        
        // Will notify the students

        return newUser;
      });

      return {
        data: user,
        message: 'User created',
      };
    } catch (e) {
      Logger.error('Creating user', e);

      throw Response.createError(Message.tryAgain, e);
    }
  }

  static async update({ params, autoFilled, authUser }, transaction = null) {
    try {
      Logger.info('Updating user');

      const { data: user } = await this.getOne({ params, autoFilled, authUser });

      if (user.email !== params.email) {
        const dbData = await Models.user.findOne({ where: { email: params.email } });
        if (dbData) {
          throw Response.createError(Message.emailAlreadyExist);
        }
      }

      await sequelize.transaction(async t => {
        transaction = transaction || t;

        const updateData = BuildUpdate(Models.user, params, autoFilled);
        Object.assign(user, updateData);
        await user.save({ transaction });

      });

      return {
        data: user,
        message: 'User updated',
      };
    } catch (e) {
      Logger.error('Updating user', e);

      throw Response.createError(Message.tryAgain, e);
    }
  }

  static async updateStatus({ params, autoFilled, authUser }, transaction = null) {
    try {
      Logger.info('Updating user status');

      const { data: user } = await UserService.getOne({ params, autoFilled, authUser });

      if (user.statusId === UserStatus.Invited) {
        throw Response.createError(Message.noPermission);
      }

      if (user.statusId !== params.statusId) {
        await sequelize.transaction(async t => {
          transaction = transaction || t;

          const updateData = BuildUpdate(Models.user, params, autoFilled);
          Object.assign(user, updateData);
          await user.save({ transaction });

          if (user.statusId === UserStatus.Active) {
            // await FirebaseUserService.enable(user.uid);
          } else {
            // await FirebaseUserService.disable(user.uid);
          }
        });
      }

      return {
        data: user,
        message: 'Status updated',
      };
    } catch (e) {
      Logger.error('Updating user status', e);

      throw Response.createError(Message.tryAgain, e);
    }
  }

  static async getAll({ params }) {
    try {
      Logger.info('Getting students');

      const offset = (params.page - 1) * params.limit;

      const users = await Models.user.findAndCountAll({
        include: [
          {
            model: Models.role,
            where: { roleId: Role.S },
            attributes: [],
            as: 'UR'
          },
        ],
        offset,
        limit: parseInt(params.limit),
      });

      const pagination = { limit: params.limit, offset, rows: users.count };

      return {
        data: users.rows,
        extra: { pagination },
      };
    } catch (e) {
      Logger.error('Getting users', e);

      throw Response.createError(Message.tryAgain, e);
    }
  }

  static async getOne({ params, autoFilled, authUser }, withSignedUrl = false) {
    try {
      Logger.info('Getting Student');

      const user = await Models.user.findOne({
        where: {
          userId: authUser.roleId === Role.A ? params.studentId : authUser.userId,
        },
        include: [
          {
            model: Models.role,
            where: { roleId: autoFilled.roleId },
            attributes: [],
            as: 'UR'
          },
        ],
      });

      if (!user) {
        throw Response.createError(Message.noUser);
      }

      if (withSignedUrl) {
        user.imageUrl = (user.image && (await Storage.signedUrl(user.image))) || null;
        delete user.image;
        delete user.statusId;
      }

      return {
        data: user,
      };
    } catch (e) {
      Logger.error('Getting user', e);

      throw Response.createError(Message.tryAgain, e);
    }
  }

  static async remove({ params, autoFilled, authUser }, transaction = null) {
    try {
      Logger.info('Deleting user');

      const { data: user } = await UserService.getOne({ params, autoFilled, authUser });

      await sequelize.transaction(async t => {
        transaction = transaction || t;

        await user.destroy({ transaction });
        user.updatedById = authUser.userId;
        await user.save({ paranoid: false, transaction });

        // await FirebaseUserService.remove(user.uid);
      });

      return {
        message: 'User deleted',
      };
    } catch (e) {
      Logger.error('Deleting user', e);

      throw Response.createError(Message.tryAgain, e);
    }
  }

  static async resetPassword({ params }) {
    try {
      Logger.info('Reset password');

      const { data: user } = await UserService.getOneByEmail(params);

      // TODO handle if user status is Invited

      // EmailService.resetPassword(user);

      return {};
    } catch (e) {
      Logger.error('Reset password', e);

      throw Response.createError(Message.tryAgain, e);
    }
  }

  static async resendVerifyEmail({ params, autoFilled, authUser }) {
    try {
      Logger.info('Resend email verification');

      const { data: user } = await UserService.getOne({ params, autoFilled, authUser });

      // EmailService.userCreate(user);

      return {};
    } catch (e) {
      Logger.error('Resend email verification', e);

      throw Response.createError(Message.tryAgain, e);
    }
  }

  static async assignSubject({ params, autoFilled, authUser }, transaction = null) {
    try {
      Logger.info('Assigning subjects to user');

      const { data: user } = await UserService.getOne({ params, autoFilled, authUser });

      const subjects = await Models.subject.findAll({
        where: {
          id: {
            [Sequelize.Op.in]: params.subjectIds,
          },
        },
      });

      if (subjects.length !== params.subjectIds.length) {
        throw Response.createError(Message.noSubject);
      }

      await sequelize.transaction(async t => {
        transaction = transaction || t;

        await user.setSubjects(subjects, { transaction, through: autoFilled });
      });

      return {};
    } catch (e) {
      Logger.error('Assigning subjects to user', e);

      throw Response.createError(Message.tryAgain, e);
    }
  }

  static async getAssignedSubjects({ params, autoFilled, authUser }) {
    try {
      Logger.info('Getting user subjects');

      const { data: user } = await UserService.getOne({ params, autoFilled, authUser });

      const subjects = await user.getSubjects({ joinTableAttributes: [] });

      return { data: subjects };
    } catch (e) {
      Logger.error('Getting user subjects', e);

      throw Response.createError(Message.tryAgain, e);
    }
  }
}

module.exports = StudentService;
