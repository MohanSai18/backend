const config = {
  AppName: process.env.APP_NAME || 'App',
  IsLocal: process.env.NODE_ENV === 'local',
  IsProd: process.env.NODE_ENV === 'prod',
  InstanceId: process.env.NODE_APP_INSTANCE || '0',

  port: parseInt(process.env.PORT, 10) || 3000,

  database: {
    host: process.env.DB_HOSTNAME,
    name: process.env.DB_NAME,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    dialect: 'mysql',
    charset: 'utf8mb4',
    collate: 'utf8mb4_unicode_ci',
  },

  /**
   * Used by winston logger
   */
  logs: {
    level: process.env.LOG_LEVEL || 'info',
  },

  ContentDir: `${process.env.PWD}/uploads/content`,

  // /**
  //  * GCP Service Account
  //  */
  // ServiceAccountConfigFile: `${process.env.PWD}/keys/${process.env.GCP_SA_FILE}`,
  // BucketName: process.env.BUCKET_NAME,
  // BucketDir: {
  //   Content: 'content',
  //   Logo: 'logo',
  //   Profile: 'profile',
  //   Question: 'question',
  // },

  // FirebaseClientConfigFile:
  //   (process.env.NODE_ENV === 'local' &&
  //     process.env.FIREBASE_CLIENT_FILE &&
  //     `${process.env.PWD}/keys/${process.env.FIREBASE_CLIENT_FILE}`) ||
  //   null,

  // Mailgun: {
  //   Domain: process.env.MAILGUN_DOMAIN,
  //   ApiKey: process.env.MAILGUN_API_KEY,
  //   VerifiedEmail: process.env.VERIFIED_EMAIL,
  // },

  ApiUrl: process.env.API_URL,
  WebUrl: process.env.WEB_URL,
  JwtSecret: process.env.JWT_SECRET,
};

module.exports = config;
